FROM scratch
MAINTAINER Duan Li <duan.li@vocus.com.au>

ADD centos-7-docker.tar.xz /

LABEL org.label-schema.schema-version="1.0" \
    org.label-schema.name="CentOS Base Image" \
    org.label-schema.vendor="Vocus" \
    org.label-schema.version="7.6.1810" \
    org.label-schema.build-date="20181214"

CMD ["/bin/bash"]
